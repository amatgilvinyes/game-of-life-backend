---
layout: home
title: Inici
permalink: /
---

# Benvolgut/da!


A n'aquesta web trobaràs un conjunt d'útils creats sense un motiu definit exacte.
S'hi accedeix amb els botons de la _sidebar_ de l'esquerra. T'invito a explorar-los, la idea és que tots siguin interessants a la seva manera.


_**Nota: Tingueu en compte que el lloc web està en Alpha, la majoria de pàgines no estan ni acabades ni polides.**_

[//]: Aquí podria posar alguna imatge? No sé quina estaria bé
